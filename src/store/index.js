import { createStore } from 'vuex';

export default createStore({
  state: {
    items: [],
  },
  getters: {},
  mutations: {
    SET_ITEMS(state, payload) {
      this.state.items = payload;
    },
  },
  actions: {},
  modules: {},
});
